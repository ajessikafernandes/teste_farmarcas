#pragma checksum "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c839061f5308c66336f62874b5c6a241fdf1aa50"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(LojasCRUD.Pages.Lojas.Pages_Lojas_DadosLojaOriginal), @"mvc.1.0.razor-page", @"/Pages/Lojas/DadosLojaOriginal.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Lojas/DadosLojaOriginal.cshtml", typeof(LojasCRUD.Pages.Lojas.Pages_Lojas_DadosLojaOriginal), null)]
namespace LojasCRUD.Pages.Lojas
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Development\src\LojasCRUD\Pages\_ViewImports.cshtml"
using LojasCRUD;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c839061f5308c66336f62874b5c6a241fdf1aa50", @"/Pages/Lojas/DadosLojaOriginal.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"815f7e7c15744f43ecbd44f7ac0d455a6977db26", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Lojas_DadosLojaOriginal : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Map", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(50, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
            BeginContext(95, 41, true);
            WriteLiteral("<div class=\"container\">\r\n    <h2><strong>");
            EndContext();
            BeginContext(137, 52, false);
#line 8 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.razaoSocial));

#line default
#line hidden
            EndContext();
            BeginContext(189, 115, true);
            WriteLiteral("</strong></h2>\r\n\r\n    <div>\r\n        <hr />\r\n        <dl class=\"dl-horizontal\">\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(305, 52, false);
#line 14 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.razaoSocial));

#line default
#line hidden
            EndContext();
            BeginContext(357, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(413, 48, false);
#line 17 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.razaoSocial));

#line default
#line hidden
            EndContext();
            BeginContext(461, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(517, 46, false);
#line 20 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.email));

#line default
#line hidden
            EndContext();
            BeginContext(563, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(619, 42, false);
#line 23 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.email));

#line default
#line hidden
            EndContext();
            BeginContext(661, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(717, 51, false);
#line 26 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.logradouro));

#line default
#line hidden
            EndContext();
            BeginContext(768, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(824, 47, false);
#line 29 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.logradouro));

#line default
#line hidden
            EndContext();
            BeginContext(871, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(927, 47, false);
#line 32 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.bairro));

#line default
#line hidden
            EndContext();
            BeginContext(974, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(1030, 43, false);
#line 35 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.bairro));

#line default
#line hidden
            EndContext();
            BeginContext(1073, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(1129, 47, false);
#line 38 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.cidade));

#line default
#line hidden
            EndContext();
            BeginContext(1176, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(1232, 43, false);
#line 41 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.cidade));

#line default
#line hidden
            EndContext();
            BeginContext(1275, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(1331, 44, false);
#line 44 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.cep));

#line default
#line hidden
            EndContext();
            BeginContext(1375, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(1431, 40, false);
#line 47 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.cep));

#line default
#line hidden
            EndContext();
            BeginContext(1471, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(1527, 43, false);
#line 50 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.uf));

#line default
#line hidden
            EndContext();
            BeginContext(1570, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(1626, 39, false);
#line 53 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.uf));

#line default
#line hidden
            EndContext();
            BeginContext(1665, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(1721, 49, false);
#line 56 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.Latitude));

#line default
#line hidden
            EndContext();
            BeginContext(1770, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(1826, 45, false);
#line 59 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.Latitude));

#line default
#line hidden
            EndContext();
            BeginContext(1871, 55, true);
            WriteLiteral("\r\n            </dd>\r\n            <dt>\r\n                ");
            EndContext();
            BeginContext(1927, 50, false);
#line 62 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayNameFor(model => model.Loja.Longitude));

#line default
#line hidden
            EndContext();
            BeginContext(1977, 55, true);
            WriteLiteral("\r\n            </dt>\r\n            <dd>\r\n                ");
            EndContext();
            BeginContext(2033, 46, false);
#line 65 "C:\Development\src\LojasCRUD\Pages\Lojas\DadosLojaOriginal.cshtml"
           Write(Html.DisplayFor(model => model.Loja.Longitude));

#line default
#line hidden
            EndContext();
            BeginContext(2079, 67, true);
            WriteLiteral("\r\n            </dd>\r\n        </dl>\r\n    </div>\r\n    <div>\r\n        ");
            EndContext();
            BeginContext(2146, 30, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "027e6fa64e724ce394390b8ba1a7cdef", async() => {
                BeginContext(2166, 6, true);
                WriteLiteral("Voltar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2176, 1258, true);
            WriteLiteral(@"
    </div>


	<!-- Latitude Longitude -->

	-18.701662
	-47.498520

	latitude: <input id=""latitude"" type=""text"" /> <br />
	longitude: <input id=""longitude"" type=""text"" /> <br />
 	<input id=""marcar"" type=""button"" value=""Marcar"" onclick=""FazMarcacao(latitude.value, longitude.value)"" />


    <strong>Confira o endereço no mapa: </strong>

        <div id=""googleMap"" style=""width:100%;height:400px;""></div>

        <script>
            function myMap(lat, long) {
                
            //var a = -18.701662;
            //var b = -47.498520;

            var x = document.getElementById('lat');
            var y = document.getElementById('lng');

            var mapLocal = {
                center: new google.maps.LatLng(z, y),
                zoom: 12,
            };
            var map = new google.maps.Map(document.getElementById(""googleMap""), mapLocal);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(x, y),
          ");
            WriteLiteral("      map: map,\r\n            });\r\n\r\n            \r\n            }\r\n        </script>\r\n\r\n        <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDOvo5YxQOGAPIQo4uqV6GhVMc5XGqVlpo&callback=myMap\"></script>\r\n\r\n\r\n\r\n\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LojasCRUD.Pages.Lojas.DetailsModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<LojasCRUD.Pages.Lojas.DetailsModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<LojasCRUD.Pages.Lojas.DetailsModel>)PageContext?.ViewData;
        public LojasCRUD.Pages.Lojas.DetailsModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
